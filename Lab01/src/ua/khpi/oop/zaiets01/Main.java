package ua.khpi.oop.zaiets01;

import java.util.Arrays;

import static java.lang.Integer.toBinaryString;

public class Main {

    public static void main(String[] args) {

        int number = 0x04925; //18725
        long phone = 380667775060L;
        byte phone_two_number = 0b111000; //56
        int phone_four_number = 17114; //7756
        float remainder_of_number = 61.12f;
        char symbol = 'A';

        Number(number);                           //
        Phone(phone);                             //
        Phone_two_number(phone_two_number);       //
        Phone_four_number(phone_four_number);     //ФУНКЦИИ ПЕРЕВОДА ЧИСЕЛ В ДВОИЧНУЮ СИСТЕМУ И ПОДСЧЕТА ПАРНЫХ И НЕПАРНЫХ ЦИФР В ЧИСЛЕ
        Remainder_of_number(remainder_of_number); //
        Symbol(symbol);                           //

        NumberBinary(number);                                       //
        PhoneBinary(phone);                                         //
        Phone_two_numberBinary((byte) phone_two_number);            //
        Phone_four_numberBinary((int) phone_four_number);           //ФУНКЦИИ ПЕРЕВОДА В ДВОИЧНУЮ СИСТЕМУ И ПОДСЧЕТА КОЛИЧЕСТВА ЕДИНИЦ В ЧИСЛЕ
        Remainder_of_numberBinary((float) remainder_of_number);     //
        SymbolBinary((char)symbol);                                 //
    }

    static void Number(int number) {

        System.out.print("Number: " + number);

        int ColEven = 0;
        int ColOdd = 0;
        int result = 0;

        while (number != 0) {
            result = number % 10;
            number /= 10;

            //System.out.print(result + " ");

            if (result % 2 == 0) {
                ColEven++;

            } else {
                ColOdd++;
            }
        }
        System.out.print("  " + "Even number: " + ColEven);
        System.out.print("  " + "Odd number: " + ColOdd + "\n");
    }

    static void Phone(long phone) {

        System.out.print("Phone: " + phone);
        int ColEven = 0;
        int ColOdd = 0;
        long result = 0;

        while (phone != 0) {
            result = phone % 10;
            phone /= 10;

            //System.out.print(result + " ");

            if (result % 2 == 0) {
                ColEven++;

            } else {
                ColOdd++;
            }
        }
        System.out.print("  " + "Even number: " + ColEven);
        System.out.print("  " + "Odd number: " + ColOdd + "\n");
    }

    static void Phone_two_number(byte phone_two_number) {

        System.out.print("Phone_two_number: " + phone_two_number);
        int ColEven = 0;
        int ColOdd = 0;
        int result = 0;

        while (phone_two_number != 0) {
            result = phone_two_number % 10;
            phone_two_number /= 10;

            //System.out.print(result + " ");

            if (result % 2 == 0) {
                ColEven++;

            } else {
                ColOdd++;
            }
        }
        System.out.print("  " + "Even number: " + ColEven);
        System.out.print("  " + "Odd number: " + ColOdd + "\n");
    }

    static void Phone_four_number(int phone_four_number) {

        System.out.print("Phone_four_number: " + phone_four_number);
        int ColEven = 0;
        int ColOdd = 0;
        int result = 0;

        while (phone_four_number != 0) {
            result = phone_four_number % 10;
            phone_four_number /= 10;

            //System.out.print(result + " ");

            if (result % 2 == 0) {
                ColEven++;

            } else {
                ColOdd++;
            }
        }
        System.out.print("  " + "Even number: " + ColEven);
        System.out.print("  " + "Odd number: " + ColOdd + "\n");
    }

    static void Remainder_of_number(float remainder_of_number) {

        System.out.print("remainder_of_number: " + remainder_of_number);
        int ColEven = 0;
        int ColOdd = 0;
        int result = 0;
        do {
            remainder_of_number *= 10;
            if (remainder_of_number % 10 == 0) {
                remainder_of_number /= 10;
                break;
            }
        } while (remainder_of_number % 10 != 0);

        int Integer;
        Integer = (int) remainder_of_number;

        while (Integer != 0) {
            result = (int) (Integer % 10);
            Integer /= 10;

            //System.out.print(result + " ");

            if (result % 2 == 0) {
                ColEven++;

            } else {
                ColOdd++;
            }
        }
        System.out.print("  " + "Even number: " + ColEven);
        System.out.print("  " + "Odd number: " + ColOdd + "\n");
    }

    static void Symbol(char symbol) {
        //int Symbol_to_Integer = Character.digit(symbol, 'A');

        System.out.print("Phone_four_number: " + symbol + "(" + (int) symbol + ")");
        int ColEven = 0;
        int ColOdd = 0;
        int result = 0;

        while (symbol != 0) {
            result = symbol % 10;
            symbol /= 10;


            if (result % 2 == 0) {
                ColEven++;

            } else {
                ColOdd++;
            }
        }
        System.out.print("  " + "Even number: " + ColEven);
        System.out.print("  " + "Odd number: " + ColOdd + "\n");
    }

    static void NumberBinary(int number) {
        char a = '1';
        String convert = Integer.toBinaryString(number);

        System.out.print("Number binary: " + convert);
        int ColEven = 0;

        for (int i = 0; i < convert.length(); i++) {
            if (convert.charAt(i) == a)
                ColEven++;
        }
        System.out.print("  " + "Number of numbers 1: " + ColEven + "\n");
    }

    static void PhoneBinary(long phone) {
        char a = '1';
        String convert = Long.toBinaryString(phone);

        System.out.print("Phone binary: " + convert);
        int ColEven = 0;

        for (int i = 0; i < convert.length(); i++) {
            if (convert.charAt(i) == a)
                ColEven++;
        }
        System.out.print("  " + "Number of numbers 1: " + ColEven + "\n");
    }

    static void Phone_two_numberBinary(byte phone_two_number) {
        char a = '1';
        //String convert = Byte.toBinaryString(phone_two_number);
        String str = String.format("%8s", Integer.toBinaryString(phone_two_number & 0xFF)).replace(' ', '0');

        System.out.print("Phone two number binary: " + str);
        int ColEven = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == a)
                ColEven++;
        }
        System.out.print("  " + "Number of numbers 1: " + ColEven + "\n");
    }

    static void Phone_four_numberBinary(int phone_four_number) {
        char a = '1';
        String convert = Integer.toBinaryString(phone_four_number);

        System.out.print("Phone four number binary: " + convert);
        int ColEven = 0;

        for (int i = 0; i < convert.length(); i++) {
            if (convert.charAt(i) == a)
                ColEven++;
        }
        System.out.print("  " + "Number of numbers 1: " + ColEven + "\n");
    }

    static void Remainder_of_numberBinary(float remainder_of_number) {
        String string1 = new String();
        String string2 = new String();

        char a = '1';
        float whole;
        double balance;
        char str1 = '0';
        char str2 = '1';
        String el1 = "";
        String el2 = "";
        String SumEl = "";

        System.out.print("Remainder of number binary: ");

        whole = (long) remainder_of_number;
        balance = remainder_of_number - whole;
        int value = (int) whole;


        for(int i = 0; i < 6; i++) {

            if (value % 2 >= 0.5) {
                // Math.floor(value);
                value -= 1;

                el1 += "1";

            }else {

                el1 += "0";
            }
            value /= 2;
        }
        StringBuffer buffer = new StringBuffer(el1);
        buffer.reverse();

        for(int i = 0; i < 4; i++){

            double result = balance *= 2;
            double finite = result -= 1;

            if(finite >= 0){
                el2 += '1';
            }else{
                el2 += '0';

            }
        }
        System.out.print(SumEl = buffer + "." + el2);

        int ColEven = 0;
        for(int i = 0; i < SumEl.length(); i++)
        {    if(SumEl.charAt(i) == a)
            ColEven++;
        }
        System.out.print("  " + "Number of numbers 1: " + ColEven + "\n");
    }

    static void SymbolBinary(char symbol){

        int SymbolToInt = symbol;
        char a = '1';
        String convert = Integer.toBinaryString(SymbolToInt);

        System.out.print("Number binary: " + convert);
        int ColEven = 0;

        for (int i = 0; i < convert.length(); i++) {
            if (convert.charAt(i) == a)
                ColEven++;
        }
        System.out.print("  " + "Number of numbers 1: " + ColEven + "\n");
    }
}